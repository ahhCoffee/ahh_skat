package guru.ahh.ahh_skat;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringReader;

import java.net.URL;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.stream.JsonParser;//http://docs.oracle.com/javaee/7/tutorial/jsonp004.htm


//
import java.net.HttpURLConnection;
/**
 *
 * @author ahh
 */
public class AHH_SKAT {

    
    public static void main(String[] args) {
        AHH_SKAT a=new AHH_SKAT();
        HttpURLConnectionExample http = new HttpURLConnectionExample();
        
        try {
            System.out.println("Testing 1 - Send Http GET request");
            String getJSON=http.sendGet();            
        
        Object[] points_token=jsonToJava(getJSON);
         int[]  points=a.points((ArrayList<int[]>)points_token[0]);
         String token=(String)points_token[1];
        
        try {System.out.println("\nTesting 2 - Send Http POST request");http.sendPost(points,token);}catch(Exception e){}
        
        }catch(Exception e){}
        
    }
    
int[] points(ArrayList<int[]> x){
int[] point=new int[x.size()==11?10:x.size()];//Antag at list x har 1-11 elementer.

for(int i=0;i<(x.size()==11?10:x.size());i++)

if(x.get(i)[0]==10)//strike
     point[i]=(i>0?point[i-1]:0)+10+
        (x.size()>i+2?
          x.get(i+1)[0]
        +(x.get(i+1)[0]==10?x.get(i+2)[0]:x.get(i+1)[1]):
       (x.size()>i+1?x.get(i+1)[0]+x.get(i+1)[1]:0));

else if(x.get(i)[0]+x.get(i)[1]==10)//spare
     point[i]=(i>0?point[i-1]:0)+10+(x.size()>i+1?x.get(i+1)[0]:0);

else //ej strike og ej spare
     point[i]=(i>0?point[i-1]:0)+x.get(i)[0]+x.get(i)[1];    

return point;
}
    
    
    
    static Object[] jsonToJava(String jsonData){
        ArrayList<int[]> l=new ArrayList();int[] n=new int[2];int i=0;
        Object[] a=new Object[2];a[0]=l;
    JsonParser parser = Json.createParser(new StringReader(jsonData));
while (parser.hasNext()) {
   JsonParser.Event event = parser.next();   
   switch(event) {
      case START_ARRAY:n=new int[2];i=0;break;
      case END_ARRAY:l.add(n);break;
      case END_OBJECT:l.remove(l.size()-1);return a;
      case VALUE_STRING:a[1]=parser.getString();break;
      case VALUE_NUMBER:n[i++]=parser.getInt();break; 
      default:break;//case START_OBJECT:break;case VALUE_FALSE:break;case VALUE_NULL:break;case VALUE_TRUE:break;case KEY_NAME:break;         
   }
} return a;
    }
    
    
    static class HttpURLConnectionExample {
        static private String post_response;static String getPost_response(){return post_response;};
        static private int responseCode;static int getResponseCode(){return responseCode;};
        
    private final String USER_AGENT = "Mozilla/5.0";
    // HTTP GET request
	private String sendGet() throws Exception {

		String url="http://95.85.62.55/api/points";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
                return response.toString();

	}

	// HTTP POST request
	private void sendPost(int[] p, String token) throws Exception {
            
		String url ="http://95.85.62.55/api/points";
		URL obj = new URL(url);
	      //HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

//http://95.85.62.55/api/points?points=[3,11,21,29,36,46]&token=cUZ44eLr1x485TsyoKYVYmyGHKe6jOzx
		//String urlParameters = "points=[30,60,90,120,150,180,210,240,270,300]=&token=eKtwoHzhEYWoMTvOxCoqsOutTikGZWI8";
StringBuilder sb=new StringBuilder("points=["+p[0]);for(int i=1;i<p.length;i++)sb.append(","+p[i]);sb.append("]&token="+token);
                 String urlParameters =sb.toString(); //String urlParameters = "points=[30,60,90,120,150,180,210,240,270,300]&token=M9kJYhJoHFjR1oPpQs1GqXvKXSchnD2N";


		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
post_response=response.toString();
	}
    }

  
}
