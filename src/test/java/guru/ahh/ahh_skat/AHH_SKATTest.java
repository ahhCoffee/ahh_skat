package guru.ahh.ahh_skat;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ahh
 */
public class AHH_SKATTest {
    public static int[][][] tD ;//testData
    static int[][][] par11 ;
            static{
        tD=new int[11][][];
    for(int i=0;i<11;i++){tD[i]=new int[11-i][2];
        for(int j=0;j<11-i;j++){tD[i][j][0]=i;tD[i][j][1]=j;}}
    //Afslut (10.runde) med strike giver ekstra 2 kast
    //{"points":[[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,10]],"token":"XcUJZZIzbprY7BTsFF8s5U7uzU2QOfOH"}
    //Afslut (10.runde) med spare giver et ekstra kast
    //Antag server returnerer 1-11 par.
     par11=new int[11][11][];//par med værdier (x,y) hvor 0<= x,y <=10.
    for(int i=0;i<11;i++)for(int j=0;j<11;j++)par11[i][j]=new int[]{i,j};
    }  

    /**
     * Test of main method, of class AHH_SKAT.
     */
    @Test
    public void testMain() {
        System.out.println("Test metode main \n\n");
        String[] args = null;
        AHH_SKAT.main(args);
        assertTrue(AHH_SKAT.HttpURLConnectionExample.getPost_response().contains("\"success\":true"));
        assertEquals(200, AHH_SKAT.HttpURLConnectionExample.getResponseCode());        
    }

    /**
     * Test of Points method, of class AHH_SKAT.
     */
    @Test
    public void testPoints() {
 System.out.println("Points");
    ArrayList<int[]> x =new ArrayList<>();
x.add(tD[5][2]);//indeholder {5,2}.
x.add(tD[5][2]);
x.add(tD[10][0]);x.add(tD[5][2]);x.add(tD[5][2]);
x.add(tD[8][2]);x.add(tD[5][2]);x.add(tD[5][2]);
//ArrayList x = null;
        AHH_SKAT instance = new AHH_SKAT();
        int[] expResult  = new int[]{7,14,31,38,45,60,67,74};
        int[] result = instance.points(x);
        assertArrayEquals(expResult, result); 
      
    }
    /**
     * Test of jsonToJava method, of class AHH_SKAT.
     */
    @Test
    public void testJsonToJava() {
        System.out.println("\n\n\nTest metode jsonToJava");
        String jsonData = "{\"points\":[[0,4],[3,3],[5,1],[10,0],[6,2]],\"token\":\"KBVV7rQiFt6M4qGx15lStJRgtb2URQDV\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[0][4]);points.add(tD[3][3]);points.add(tD[5][1]);points.add(tD[10][0]);points.add(tD[6][2]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="rhdGwZCxloBMQqeUP3oz2DPO94fQQAK3";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    }
    
    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     */
    @Test
    public void testJsonToJavaThenPoints() {
        System.out.println("jsonToJavaThenPoints");
        String jsonData = "{\"points\":[[0,4],[3,3],[5,1],[10,0],[6,2]],\"token\":\"KBVV7rQiFt6M4qGx15lStJRgtb2URQDV\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[0][4]);points.add(tD[3][3]);points.add(tD[5][1]);points.add(tD[10][0]);points.add(tD[6][2]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="KBVV7rQiFt6M4qGx15lStJRgtb2URQDV";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{4,10,16,16+10+6+2,34+8};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     */
    @Test
    public void testJsonToJavaThenPoints2() {
        System.out.println("jsonToJavaThenPoints2");
        String jsonData = "{\"points\":[[3,3],[2,1],[7,0],[2,8],[9,1],[8,1],[0,10],[7,3],[1,3]],\"token\":\"tUhB5G8niRP5mvt7KhupV14gpf0WevIS\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[3][3]);points.add(tD[2][1]);points.add(tD[7][0]);points.add(tD[2][8]);points.add(tD[9][1]);points.add(tD[8][1]);points.add(tD[0][10]);points.add(tD[7][3]);points.add(tD[1][3]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="tUhB5G8niRP5mvt7KhupV14gpf0WevIS";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{6,9,16,16+10+9,35+10+8,53+9,62+10+7,79+10+1,90+4};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     */
    @Test
    public void testJsonToJavaThenPoints3() {
        System.out.println("jsonToJavaThenPoints3");
        String jsonData = "{\"points\":[[2,1],[1,5],[3,0],[4,3],[1,2],[5,2]],\"token\":\"X3PyzE4W95xI4Idd1FOsEVtaR0Yo5sHg\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[2][1]);points.add(tD[1][5]);points.add(tD[3][0]);points.add(tD[4][3]);points.add(tD[1][2]);points.add(tD[5][2]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="X3PyzE4W95xI4Idd1FOsEVtaR0Yo5sHg";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{3,9,12,19,22,29};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     */
    @Test
    public void testJsonToJavaThenPoints4() {
        System.out.println("jsonToJavaThenPoints4");
        String jsonData = "{\"points\":[[7,3],[5,3],[10,0],[2,7],[6,4],[6,3],[9,1],[7,1],[9,1],[9,1],[8,0]],\"token\":\"msJVl9oxzFQI2ameUpxxMnlBW3yhbo4s\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[7][3]);points.add(tD[5][3]);points.add(tD[10][0]);points.add(tD[2][7]);points.add(tD[6][4]);points.add(tD[6][3]);points.add(tD[9][1]);points.add(tD[7][1]);points.add(tD[9][1]);points.add(tD[9][1]);
        points.add(par11[8][0]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="msJVl9oxzFQI2ameUpxxMnlBW3yhbo4s";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{15,15+8,23+10+2+7,42+9,51+10+6,67+9,76+10+7,93+8,101+10+9,120+10+8};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     * Corner Case: Ej 10 runder og ej 11. par. Sidste runde er Spare.
     */
    @Test
    public void testJsonToJavaThenPoints5() {
        System.out.println("jsonToJavaThenPoints5");
        String jsonData = "{\"points\":[[0,8],[3,2],[8,2],[8,2],[10,0],[4,5],[1,9]],\"token\":\"kBHefLljg6TLiIcYjMwSNgKrC9Eijbs6\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[0][8]);points.add(tD[3][2]);points.add(tD[8][2]);points.add(tD[8][2]);points.add(tD[10][0]);points.add(tD[4][5]);points.add(tD[1][9]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="kBHefLljg6TLiIcYjMwSNgKrC9Eijbs6";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{8,13,13+10+8,31+10+10,51+10+4+5,70+9,79+10};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    @Test
    public void testJsonToJavaThenPoints6() {
        System.out.println("jsonToJavaThenPoints6");
        String jsonData = "{\"points\":[[8,1],[0,0]],\"token\":\"adoVS10wDTOMp6onEr7GEuJ9EbIuBtJi\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[8][1]);points.add(tD[0][0]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="adoVS10wDTOMp6onEr7GEuJ9EbIuBtJi";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{9,9};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     * Corner Case: Ej 10 runder og ej 11. par. Sidste runde er strike.
     */
    @Test
    public void testJsonToJavaThenPoints7() {
        System.out.println("jsonToJavaThenPoints7");
        String jsonData = "{\"points\":[[3,5],[9,0],[6,4],[3,6],[8,2],[9,1],[3,1],[8,1],[10,0]],\"token\":\"A7RC0X69oVqHdkb8l85Ee37iBwWyz1rI\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[3][5]);points.add(tD[9][0]);points.add(tD[6][4]);points.add(tD[3][6]);points.add(tD[8][2]);points.add(tD[9][1]);points.add(tD[3][1]);points.add(tD[8][1]);points.add(tD[10][0]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="A7RC0X69oVqHdkb8l85Ee37iBwWyz1rI";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{8,17,17+10+3,30+9,39+10+9,58+10+3,71+4,75+9,84+10};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    @Test
    public void testJsonToJavaThenPoints8() {
        System.out.println("jsonToJavaThenPoints8");
        String jsonData = "{\"points\":[[10,0],[6,0],[10,0],[5,2],[3,4],[4,2]],\"token\":\"sq1g2IalKs9lWMhOqyHEdbTbkc7CkXA7\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();points.add(tD[10][0]);points.add(tD[6][0]);points.add(tD[10][0]);points.add(tD[5][2]);points.add(tD[3][4]);points.add(tD[4][2]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="sq1g2IalKs9lWMhOqyHEdbTbkc7CkXA7";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{16,22,22+10+7,39+7,46+7,53+6};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }

    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     * Perfect score.
     */
    @Test
    public void testJsonToJavaThenPoints9() {
        System.out.println("jsonToJavaThenPoints9");
        String jsonData = "{\"points\":[[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,10]],\"token\":\"2wC3BH2LOMaUEdQCM1wdEUMdehaYsCoA\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();for(int i=0;i<10;i++)points.add(tD[10][0]); points.add(par11[10][10]);
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="2wC3BH2LOMaUEdQCM1wdEUMdehaYsCoA";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{30,60,90,120,150,180,210,240,270,300};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }

    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     * 10 strikes, men 10 par.
     */
    @Test
    public void testJsonToJavaThenPoints10() {
        System.out.println("\n\n10. test: jsonToJavaThenPoints10");
        String jsonData = "{\"points\":[[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0]],\"token\":\"2wC3BH2LOMaUEdQCM1wdEUMdehaYsCoA\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();for(int i=0;i<10;i++)points.add(tD[10][0]); 
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="2wC3BH2LOMaUEdQCM1wdEUMdehaYsCoA";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{30,60,90,120,150,180,210,240,260,270};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    /**
     * Test of jsonToJava method Then method points, of class AHH_SKAT.
     * 10 par. Sidste er spare.
     */
    @Test
    public void testJsonToJavaThenPoints11() {
        System.out.println("11. test: jsonToJavaThenPoints11");
        String jsonData = "{\"points\":[[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[10,0],[0,10]],\"token\":\"2wC3BH2LOMaUEdQCM1wdEUMdehaYsCoA\"}";
        Object[] result = AHH_SKAT.jsonToJava(jsonData);
        ArrayList<int[]> points=new ArrayList();for(int i=0;i<9;i++)points.add(tD[10][0]); points.add(tD[0][10]); 
        Object[] expResult=new Object[2];expResult[0]=points;expResult[1]="2wC3BH2LOMaUEdQCM1wdEUMdehaYsCoA";
        for(int i=0;i<points.size();i++)assertArrayEquals(((ArrayList<int[]>)expResult[0]).get(i), ((ArrayList<int[]>)result[0]).get(i));        
    
    AHH_SKAT instance = new AHH_SKAT();
    int[] expResultInstancePoints  = new int[]{30,60,90,120,150,180,210,230,230+10+0+10,250+10};
    int[] resultInstancePoints = instance.points(points);
    assertArrayEquals(expResultInstancePoints, resultInstancePoints);
    }
    
    
    
    @Test
    public void testMyMethod2() {
 System.out.println("myMethod 2");
    ArrayList<int[]> x =new ArrayList<>();
x.add(tD[5][2]);//indeholder {5,2}.
x.add(tD[5][2]);
x.add(tD[10][0]);x.add(tD[5][2]);x.add(tD[5][2]);
x.add(tD[8][2]);x.add(tD[5][2]);
x.add(tD[5][5]);//spare men ikke muligt at læse næste værdi, da ikke fuldt spil

        AHH_SKAT instance = new AHH_SKAT();
        int[] expResult  = new int[]{7,14,31,38,45,60,67,77};
        int[] result = instance.points(x);
        assertArrayEquals(expResult, result); 
   
    }
    
     @Test
    public void testMyMethod3() {
 System.out.println("myMethod 3");
    ArrayList<int[]> x =new ArrayList<>();
x.add(tD[5][2]);//indeholder {5,2}.
x.add(tD[5][2]);
x.add(tD[10][0]);x.add(tD[5][2]);x.add(tD[5][2]);
x.add(tD[8][2]);x.add(tD[5][2]);
x.add(tD[10][0]);//strike men ikke muligt at læse næste værdi, da ikke fuldt spil.

        AHH_SKAT instance = new AHH_SKAT();
        int[] expResult  = new int[]{7,14,31,38,45,60,67,77};
        int[] result = instance.points(x);
        assertArrayEquals(expResult, result); 
         
    }
    
    @Test
    public void testMyMethod4() {
 System.out.println("myMethod 4");
    ArrayList<int[]> x =new ArrayList<>();
x.add(tD[5][2]);//indeholder {5,2}.
        AHH_SKAT instance = new AHH_SKAT();
        int[] expResult  = new int[]{7};
        int[] result = instance.points(x);
        assertArrayEquals(expResult, result); 
       
    }

@Test public void testMyMethod5() {ArrayList<int[]> x =new ArrayList<>();x.add(tD[10][0]);AHH_SKAT instance = new AHH_SKAT();int[] expResult  = new int[]{10};int[] result = instance.points(x);assertArrayEquals(expResult, result);}
@Test public void testMyMethod6() {ArrayList<int[]> x =new ArrayList<>();x.add(tD[7][3]);AHH_SKAT instance = new AHH_SKAT();int[] expResult  = new int[]{10};int[] result = instance.points(x);assertArrayEquals(expResult, result);}

@Test public void testMyMethod7() {ArrayList<int[]> x =new ArrayList<>();for(int i=0;i<10;i++)x.add(tD[10][0]);
x.add(par11[10][10]);
AHH_SKAT instance = new AHH_SKAT();int[] expResult  = new int[]{30,60,90,120,150,180,210,240,270,300};int[] result = instance.points(x);assertArrayEquals(expResult, result);}

@Test public void testMyMethod8() {ArrayList<int[]> x =new ArrayList<>();x.add(tD[10][0]);x.add(tD[10][0]);x.add(tD[10][0]);x.add(tD[10][0]);x.add(tD[10][0]);x.add(tD[10][0]);x.add(tD[10][0]);x.add(tD[10][0]);x.add(tD[10][0]);
x.add(tD[9][1]);//spare i sidste runde
x.add(par11[7][0]);//0 fordi kun et ekstra kast, og antager modtager par, så 2.-kordinat i par11 er altid 0 når spare i sidste runde.
AHH_SKAT instance = new AHH_SKAT();int[] expResult  = new int[]{30,60,90,120,150,180,210,210+10+10+9,239+10+9+1,259+10+7};int[] result = instance.points(x);assertArrayEquals(expResult, result);}

}